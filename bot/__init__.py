import requests
from bs4 import BeautifulSoup
from . import conf
import re

def get_referece_date(str):
    return re.search(u'(\d+).(\d+).(\d+)', str).group()


def run():
    quotes = []

    response = requests.get(conf.URL_CURRENCY_QUOTES)
    soup = BeautifulSoup(response.text, 'html.parser')

    date = get_referece_date(soup.find('strong').text)

    for row in soup.find('table').find_all('tr'):
        cols = row.find_all('td')
        if len(cols) > 0:
            quotes.append({
                'currencyCode': cols[0].text,
                'currencyType': cols[1].text,
                'currencyName': cols[2].text,
                'currencyBuyRate': cols[3].text,
                'currencySellRate': cols[4].text,
                'currencyBuyParity': cols[5].text,
                'currencySellParity': cols[6].text
            })